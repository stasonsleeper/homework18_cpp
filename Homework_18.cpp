// Homework_18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <stack>

using namespace std;

class Stack
{
private:
 
    int asize = 1;
    int *ar = new int[asize];
    int x = 0;

public:

    Stack() : asize(0)
    {}
    Stack(int asize) : asize(asize)
    {}
    
    int Push(int c) // adds new element in array
    {
        
        if (x < asize)
        {
            ar[x] = c;
            cout << "You added new element in Array (" << c << ")" << endl;
            cout << "Array element of " << x << " is " << ar[x] << endl;
            x++;
            asize = asize + x;
            return 0;
        }
        else 
        {
            cout << "X is more than asize attribute. Please, check your code." << endl;
        }
        
    }

    void Pop() // shows top element of array
    {
        if (x > 0)
        {
            x--;
            cout << "The top element of Array is " << ar[x] << endl;
        }
        else
        {
            cout << "Your Array is empty. Please, fill it up." << endl;
        }
    }

    void ShowArray()
    {
        cout << ar[x] << endl;
    }

    ~Stack()
    {
        delete[] ar;
    }
};

int main()
{
    // � ������� ������������� �������
    
    cout << "Dynamic array" << endl;
    
    Stack R(1);
    R.Push(6);
    R.Push(11);
    R.Push(9);
    R.Pop();
    R.Pop();
    
    cout << "Array task done" << endl;

    //�� �������

    stack <int> test;
    int i = 0;
    int size;
    cout << "Please enter size of your stack" << endl;
    cin >> size;
    cout << "Your stack size is " << size << ". Now enter every single stack element" << endl;
                                                       
    while (i != size) {
        int a;
        cin >> a;
        test.push(a);
        i++;
    };

    cout << "Stack element from top: " << test.top() << endl;
    test.pop();
    cout << "Second stack element from top: " << test.top() << endl;

    std::cout << "Hello World!\n";
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
